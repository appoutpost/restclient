function getUser(id) {
    $.ajax({
        url: "https://reqres.in/api/users/" + id,
        type: "GET",
        success: function (response) {
            var userPanel = $(".users");
            var data = response.data;
            var user = '<div class="user-row"><div id="firstName">' + data.first_name + '</div><div id="lastName">' + data.last_name + '</div><div id="avatar"><img class="image" width="70" height="70" src="' + data.avatar + '"> </img></div></div>';
            userPanel.append(user);
        }
    });
};

function getUsers() {
    $.ajax({
        url: "https://reqres.in/api/users/",
        type: "GET",
        success: function (response) {
            var data = response.data;
            var userPanel = $(".users");

            for (var i = 0; i < data.length; i++) {
                var user = '<div class="user-row"><div id="firstName">' + data[i].first_name + '</div><div id="lastName">' + data[i].last_name + '</div><div id="avatar"><img class="image" width="70" height="70" src="' + data[i].avatar + '"> </img></div></div>';
                userPanel.append(user);
            }
        }
    });
};

getUser(12);
getUsers();


function getBooks() {
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/posts/",
        type: "GET",
        success: function (response) {
            var data = response;
            var booksPanel = $(".books");

            for (var i = 0; i < data.length; i++) {
                var book = '<div class="book-row"><div id="id">'+data[i].id+'</div><div id="title">' + data[i].title + '</div><div id="description">' + data[i].body + '</div></div>';
                booksPanel.append(book);
            }
        }
    });
}

getBooks();